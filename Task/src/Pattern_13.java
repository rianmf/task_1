
public class Pattern_13 {

	public static void main(String[] args) {
		Integer n = 5;
		Integer nJ = n;
		Integer nI = (n * 2) - 1;
		String[][] pola = new String[nI][nJ];
		Integer nTengah = nI / 2;

		for (int i = 0; i < nI; i++) {
			for (int j = 0; j < nJ; j++) {
//	                if (i >= j && i-j <= nTengah) { // JAJAR GENJANG
				if (i > j && i + j < nI - 1) {
					pola[i][j] = " ";
				} else {
					pola[i][j] = "*";
				}
			}

		}

		// 3. Cetak
		for (int i = 0; i < nI; i++) {
			for (int j = 0; j < nJ; j++) {
				System.out.printf("%3s", pola[i][j]);
			}
			System.out.println("");
		}
	}
}