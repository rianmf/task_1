public class Pattern_4 {
    public static void main(String[] args) {
        Integer n = 6;
        String[][] pola = new String[n][n];

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                if (i < j) {
                    pola[i][j] = "*";
                } else {
                    pola[i][j] = "";
                }
            }
        }

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.printf("%3s", pola[i][j]);
            }
            System.out.println("");
        }

    }
}
