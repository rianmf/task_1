
public class Pattern_24 {

	public static void main(String[] args) {
		Integer n = 5;
		Integer nI = n;
		Integer nJ = (n*2) - 1;
		String[][] pola = new String[nI][nJ];
		Integer nTengah = nJ / 2;

		for (int i = 0; i < nI; i++) {
			for (int j = 0; j < nJ; j++) {
				if (i == 0 && j >= nTengah) {
					pola[i][j] = "*";
				} else if (i + j == nTengah && j <= nTengah) {
					pola[i][j] = "*";
				} else if (i + j == nJ-1 && j >= n) {
					pola[i][j] = "*";
				} else if (i == n-1 && j <= nTengah) {
					pola[i][j] = "*";
				} else {
					pola[i][j] = " ";
				}
			}

		}

		for (int i = 0; i < nI; i++) {
			for (int j = 0; j < nJ; j++) {
				System.out.printf("%3s", pola[i][j]);
			}
			System.out.println("");
		}
	}
}