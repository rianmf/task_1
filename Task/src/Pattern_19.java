
public class Pattern_19 {

	public static void main(String[] args) {
		Integer n = 5;
		Integer nI = n;
		Integer nJ = (n * 2) -1;
		Integer nTengah = nJ / 2;
		String[][] pola = new String[nI][nJ];

		for (int i = 0; i < nI; i++) {
			for (int j = 0; j < nJ; j++) {
				if (i + j == nI - 1 && j <= nTengah) {
					pola[i][j] = "*";
				} else if (i == nI - 1) {
					pola[i][j] = "*";
				} else if (j >= n && j - i == nTengah) {
					pola[i][j] = "*";
				} else {
					pola[i][j] = " ";
				}

			}
		}

		for (int i = 0; i < nI; i++) {
			for (int j = 0; j < nJ; j++) {
				System.out.printf("%2s", pola[i][j]);
			}
			System.out.println("");
		}

	}
}
