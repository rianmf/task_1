
public class Pattern_20 {

	public static void main(String[] args) {
		Integer n = 5;
		Integer nI = n;
		Integer nJ = (n * 2) - 1 ;
		Integer nTengah = nJ / 2;
		String[][] pola = new String[nI][nJ];

		for (int i = 0; i < nI; i++) {
			for (int j = 0; j < nJ; j++) {
				if (i == 0) {
					pola[i][j] = "*";
				} else if (i+j == nJ-1) {
					pola[i][j] = "*";
				} else if (i == j) {
					pola[i][j] = "*";
				} else {
					pola[i][j] = " ";
				}

			}
		}

		for (int i = 0; i < nI; i++) {
			for (int j = 0; j < nJ; j++) {
				System.out.printf("%3s", pola[i][j]);
			}
			System.out.println("");
		}
	}
}