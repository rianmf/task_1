
public class Pattern_27 {


	public static void main(String[] args) {
		Integer n = 9;
		Integer ni = n;
		Integer nj = n;
		String[][] pola = new String[ni][nj];
	

		for (int i = 0; i < ni; i++) {
			for (int j = 0; j < nj; j++) {
				if (i + j > 4 && j - i < 4 && i - j < 4 && j + i < 12) {
					pola[i][j] = " ";
				} else {
					pola[i][j] = "*";
				}
			}

		}

		for (int i = 0; i < ni; i++) {
			for (int j = 0; j < nj; j++) {
				System.out.printf("%2s", pola[i][j]);
			}
			System.out.println();
		}
	}

}
