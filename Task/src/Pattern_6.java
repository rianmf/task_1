
public class Pattern_6 {

	public static void main(String[] args) {
		Integer n = 9;
        Integer nI = (n+1) / 2;
        Integer nJ = n;
        String[][] pola = new String[nI][nJ];
        Integer nTengah = n/2;
      
        
        for (int i = 0; i < nI; i++) {
            for (int j = 0; j < nJ; j++) {
                if (j>=i && i+j<=nTengah) {
                    pola[i][j] = "*";
                } else {
                    pola[i][j] = "";
                }
            }

        }


        // 3. Cetak
        for (int i = 0; i < nI; i++) {
            for (int j = 0; j < nJ ; j++) {
                System.out.printf("%3s", pola[i][j]);
            }
            System.out.println("");
        }
    }
}